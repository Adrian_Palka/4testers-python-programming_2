import random
import string


# New task
def sum_of_numbers_in_a_list(a):
    return a[0] + a[1] + a[2] + a[3]


def sum_of_numbers_in_a_list2(input_list):
    return sum(input_list)


def average_of_2_numbers(c, d):
    return (c + d) / 2


# New task 2
def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return "".join(random.choice(characters) for i in range(8))


def generate_login_data_for_email(email):
    random_password = generate_random_password()
    return {"email": email, "password": random_password}


# Task warrior
def defining_the_warrior():
    return {"nick": "maestro_54", "type": "warrior", "exp_points": 3000}
    print("Trololo")


if __name__ == '__main__':
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    a = january
    b = february
    print(sum_of_numbers_in_a_list(a))
    print(sum_of_numbers_in_a_list(b))
    average_of_january = sum_of_numbers_in_a_list(a) / len(a)
    average_of_february = sum_of_numbers_in_a_list(b) / len(b)
    print(average_of_2_numbers(average_of_january, average_of_february))

    print(generate_login_data_for_email("adrian@trololo.pl"))
    defining_the_warrior()
