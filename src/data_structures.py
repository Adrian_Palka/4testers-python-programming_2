movies = ["Footsoldier", "Star Wars", "Lord of the Rings", "Vikings", "Ironclad"]
last_movie = movies[-1]
movies.append("Titanic")
movies.append("Hobbit")
print(len(movies))
middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, "Top Gun 2")
print(movies)

# Task no 1
emails = ["a@exemple.com", "b@example.com"]

print(len(emails), emails[0], emails[-1])
emails.append("cde@example.com")

# Another task
friend = {
    "name": "Adrian",
    "age": 35,
    "Hobies": ["financial education", "healthy lifestyle"]
}

print(friend)

friend_hobbies = friend["Hobies"]
print(F"My friend's hobbies{friend_hobbies}")
print(F"My friend has {len(friend_hobbies)} hobbies")
friend["Hobies"].append("football")
print(friend)
friend["married"] = True
friend["age"] = 35 + 3
print(friend)
