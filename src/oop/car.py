from datetime import date


class Car:
    def __init__(self, model, year):
        self.model = model
        self.year = year
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def has_warranty(self):
        current_year = date.today().year
        car_age = current_year - self.year
        return False if car_age > 7 or self.mileage > 120000 else True

    def get_description(self):
        return f"This is a {self.model} made in {self.year}. Currently is drove {self.mileage} kilometers"


if __name__ == '__main__':
    ford1 = Car("Fiesta", 2021)
    ford2 = Car("Focus", 2022)

    ford1.drive(25000)
    ford2.drive(160000)

    print(ford1.has_warranty())
    print(ford2.has_warranty())

    print(ford1.get_description())
    print(ford2.get_description())
