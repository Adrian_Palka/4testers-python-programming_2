class Dog:
    def __init__(self):
        self.name = "Dianka"
        self.age = "0"
        self.food_in_belly = 0

    def feed(self, amount_of_food):
        self.food_in_belly += amount_of_food

    def poop(self, weight_of_poopy):
        self.food_in_belly -= weight_of_poopy


class Dog2:
    def __init__(self, name):
        self.name = name
        self.age = "0"


if __name__ == '__main__':
    dog1 = Dog()
    dog2 = Dog()
    print(dog1.name)
    print(dog2.name)

    dog1.name = "Pączuś"
    dog2.name = "Nestorek"
    print(dog1.name)
    print(dog2.name)

    dog3 = Dog2("Troluś")
    dog4 = Dog2("Mały")
    print(dog3.name)
    print(dog4.name)

    print(dog1.food_in_belly)
    dog1.feed(100)
    print(dog1.food_in_belly)
    dog1.feed(100)
    print(dog1.food_in_belly)
    dog1.poop(50)
    print(dog1.food_in_belly)
