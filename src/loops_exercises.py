from random import randint
from math import sqrt


def print_hello_40_times():
    for i in range(0, 40):
        print("Hello", i)


def print_positive_numbers_from_1_to_30():
    for i in range(1, 31):
        print(i)


def print_positive_numbers(start, stop):
    for i in range(start, stop + 1):
        print(i)


def print_positive_numbers_divisible_by_7(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:
            print(i)


def print_n_random_numbers(n):
    for i in range(0, n):
        print(f"loop {i}:", randint(1, 1000))


def print_square_roots_of_numbers(list_of_numbers):
    for number in list_of_numbers:
        print(sqrt(number))


def get_square_roots_of_numbers(list_of_numbers):
    square_roots = []
    for number in list_of_numbers:
        sqrt_of_number = round(sqrt(number), 2)
        square_roots.append(sqrt(number))
    return square_roots


def get_list_of_temperature_in_farenheit(temps_celsius):
    temperature_in_farenheit = []
    for temp in temps_celsius:
        temp_f = temp * 9 / 5 + 32
        temperature_in_farenheit.append(temp_f)
    return temperature_in_farenheit


if __name__ == '__main__':
    print_hello_40_times()
    print_positive_numbers_from_1_to_30()
    print_positive_numbers(1, 30)
    print_positive_numbers_divisible_by_7(1, 30)
    print_n_random_numbers(20)
    print(sqrt(9))
    print(sqrt(16))
    names = ["Jim", "Pat", "Anna", "Adrian"]
    list_of_measurement_results = [11.0, 123, 69, 80, 43, 23, 1, 5, 77]
    print_square_roots_of_numbers(list_of_measurement_results)
    square_roots_of_measurements = get_square_roots_of_numbers(list_of_measurement_results)
    print(square_roots_of_measurements)

    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(get_list_of_temperature_in_farenheit(temps_celsius))