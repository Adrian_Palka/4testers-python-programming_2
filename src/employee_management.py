import random
import time
import uuid


def generate_random_number():
    return random.randint(0, 40)


def generate_random_email():
    domain = "example.com"
    names_list = ["james", "kate", "Adrian", "Tomek"]
    random_name = random.choice(names_list)
    # suffix = time.time()
    # suffix = random.randint(1, 1_000_00)
    suffix = str(uuid.uuid4())
    return f"{random_name}.{suffix}@{domain}"


def generate_random_boolean():
    return random.choice([True, False])


def get_dictionary_with_random_personal_data():
    random_email = generate_random_email()
    random_number = generate_random_number()
    random_boolean = generate_random_boolean()
    return {
        "email": random_email,
        "seniority_years": random_number,
        "female": random_boolean,
    }


def generate_list_of_dictionaries_with_random_personal_data(number_of_dictionaries):
    list_of_dictionaries = []
    for number in range(number_of_dictionaries):
        list_of_dictionaries.append(get_dictionary_with_random_personal_data())
    return list_of_dictionaries


# Task no 3
def generate_emails_of_workers_with_seniority_years_above_10(list_of_employees):
    senior_employees = []
    for employee in list_of_employees:
        if employee["seniority_years"] > 10:
            senior_employees.append(employee["email"])
    return senior_employees


def get_emails_of_workers_with_seniority_years_above(list_of_employees, minimum_years_margin):
    senior_employees = []
    for employee in list_of_employees:
        if employee["seniority_years"] > minimum_years_margin:
            senior_employees.append(employee["email"])
    return senior_employees


def get_list_of_employees_by_their_gender_female(list_of_employees):
    filtered_employees = []
    for employee in list_of_employees:
        if employee["female"] == True:
            filtered_employees.append(employee)
    return filtered_employees


def get_list_of_employees_by_their_gender_male(list_of_employees):
    filtered_employees = []
    for employee in list_of_employees:
        if employee["female"] == False:
            filtered_employees.append(employee)
    return filtered_employees


if __name__ == '__main__':
    print(get_dictionary_with_random_personal_data())

    print(generate_list_of_dictionaries_with_random_personal_data(5))

    test_employee_list = generate_list_of_dictionaries_with_random_personal_data(5)
    print(test_employee_list)
    test_senior_employee_emails = generate_emails_of_workers_with_seniority_years_above_10(test_employee_list)
    print(test_senior_employee_emails)

    test_senior_employee_emails_2 = get_emails_of_workers_with_seniority_years_above(test_employee_list, 15)
    print(test_senior_employee_emails_2)

    test_female_employees = get_list_of_employees_by_their_gender_female(test_employee_list)
    print(test_female_employees)

    test_male_employees = get_list_of_employees_by_their_gender_male(test_employee_list)
    print(test_male_employees)
