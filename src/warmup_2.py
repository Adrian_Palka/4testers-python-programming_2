def volume_of_a_brick(a, b, c):
    return a * b * c


def square(number):
    return number ** 2


def celius_to_farenheit(temp_c):
    return temp_c * (9 / 5) + 32


if __name__ == '__main__':
    # Task no 1
    square_of_0 = square(0)
    square_of_16 = square(16)
    square_of_2_55 = square(2.55)
    print(square_of_0, square_of_16, square_of_2_55)

    # Task no 2
    celius_20_to_farenheit = celius_to_farenheit(20)
    print(celius_20_to_farenheit)

    # Task no 3
    print(volume_of_a_brick(3, 5, 7))
