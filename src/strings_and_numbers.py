first_name = "Adrian"
last_name = "Pałka"
email = "adrian.p.palka@gamil.com"

print("Mam na imię ", first_name, ". ", "Moje nazwisko to ", last_name, ".", sep="")

my_bio = "Mam na imię " + first_name + ". Moje nazwisko to " + last_name + ". Mój email to " + email + "."
print(my_bio)

# F-string
my_bio_using_f_string = f"Mam na imię {first_name}.\nMoje nazwisko to {last_name}.\nMój email to {email}."
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 to {4*5}.")

### Algebra ###
area_of_circle_with_radius_5 = 3.14 * 5 ** 2
print(f"Area of circle with radius 5: {area_of_circle_with_radius_5}.")

long_mathematical_expression = 2 + 3 + 5 * 7 + 4 / 3 * (3 + 5 / 2) + 7**2 - 13
print(long_mathematical_expression)

