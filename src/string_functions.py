def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło Cię widzieć w naszym mieście: {city}!")

def create_4testers_emails(name, surname):
    name = name.lower()
    surname = surname.lower()
    surname = surname.replace("ł","l")
    return F"{name}{surname}4testers.pl"

if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Adrian", "Budapest")
    print_greetings_for_a_person_in_the_city("Dianka", "Świdnik")
    print(create_4testers_emails("Adrian", "Pałka"))
    print(create_4testers_emails("Janusz", "Nowak"))
    print(create_4testers_emails("Barbara", "Kowalska"))
