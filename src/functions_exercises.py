def print_a_car_brand_name():
    print("Honda")


print_a_car_brand_name()


def print_given_number_multiplied_by_3(imput_number):
    print(imput_number * 3)


print_given_number_multiplied_by_3(10)


def calculate_area_of_a_circle(radius):
    return 3.1415 * radius ** 2


calculate_area_of_a_circle(10)
calculate_area_of_a_circle_with_radius_10 = calculate_area_of_a_circle(10)
print(calculate_area_of_a_circle_with_radius_10)

def calculate_area_of_a_traingle(bottom, height):
    return 0.5 * bottom * height

area_a_little_triangle = calculate_area_of_a_traingle(5, 5)
print(area_a_little_triangle)

# Checking what is returned from a function by default
result_of_a_print_function = print_a_car_brand_name()
print(result_of_a_print_function)
