# Homework
import random
from datetime import date

list_of_male_names = ["Adrian", "Piotr", "Krzysztof", "Andrzej", "Paweł"]
list_of_female_names = ["Monika", "Kasia", "Magda", "Iga", "Ela"]
list_of_surnames = ["Pałka", "Kowalski", "Nowak", "Polak", "Węgier", "Słowak", "Czech"]
list_of_countries = ["Polska", "Węgry", "Słowacja", "Czechy", "Rumunia", "Gruzja"]
min_age = 5
max_age = 45

list_of_dictionaries = []


def generate_email_address(name, surname):
    return f"{name.lower()}.{surname.lower()}@example.pl"


def generate_random_age():
    return random.randint(min_age, max_age)


def check_if_person_is_an_adult(age):
    return True if age >= 18 else False


def compute_the_birth_age(age):
    return date.today().year - age


def generate_person_data(male):
    first_name = random.choice(list_of_male_names) if male else random.choice(list_of_female_names)
    surname = random.choice(list_of_surnames)
    person_age = generate_random_age()
    return {
        "first_name": first_name,
        "surname": surname,
        "country": random.choice(list_of_countries),
        "email": generate_email_address(first_name, surname),
        "age": person_age,
        "adult": check_if_person_is_an_adult(person_age),
        "birth_year": compute_the_birth_age(person_age)
    }


def generate_list_of_people(number_of_people):
    list_of_people = []
    for i in range(number_of_people):
        male = bool(i % 2)
        person_dictionary = generate_person_data(male)
        list_of_people.append(person_dictionary)
    return list_of_people


def generate_and_print_list_of_people_welcome_message(people_list):
    for person in people_list:
        print(f"Hi! I'm {person['first_name']}. I come from {person['country']} and I was born in {person['country']}.")

if __name__ == '__main__':
    people = generate_list_of_people(10)
    generate_and_print_list_of_people_welcome_message(people)
