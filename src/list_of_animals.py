animals = [
    {"name": "Dianka", "kind": "dog", "age": 16},
    {"name": "Bonifacy", "kind": "cat", "age": None},
    {"name": "Misio", "kind": "hamster", "age": 1},
]
print(animals[-1]["name"])
animals[1]["age"] = 2
print(animals[1])
animals.append({"name": "Pączek", "kind": "dog", "age": 2})
animals.insert(0, {"name": "Reksio", "kind": "dog", "age": 12})
print(animals)

list_of_addresses = [
    {"city": "Świdnik", "street": "Słowackiego", "house_number": 10, "post_code": "21-040"},
    {"city": "Łódź", "street": "Lumumby", "house_number": 12, "post_code": "21-050"},
    {"city": "Budapest", "street": "Orban Victor", "house_number": 10, "post_code": "21-060"}
]

print(list_of_addresses[-1]["post_code"])
print(list_of_addresses[1]["city"])
list_of_addresses[0]["street"] = "Mickiewicza"
print(list_of_addresses)