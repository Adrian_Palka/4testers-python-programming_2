def display_speed_information(speed):
    if speed <= 50:
        print("Thank you, your speed is below limit! :)")
    else:
        print("Slow down, please!")


# Task
def temperature_and_air_pressure_check(temp, air_p):
    if temp == 0 and air_p == 1013:
        return True
    else:
        return False

def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print("Your speed was:", speed)
    if speed > 100:
        print(f"You have just lost your driving license! Fine amount:{calculate_fine_amount(speed)}")
    elif speed > 50:
        print(f"You have just got a fine! ;( Fine amount:{calculate_fine_amount(speed)}")
    else:
        print("Your speed is OK ;)")

def calculate_fine_amount(speed):
    return 500 + (speed -50) *10

# Another Task
def marks(score):
    if not (isinstance(score, float) or isinstance(score, int)):
        return "Nieprawidłowe znaki, wpisz od 2.0 do 5.0!"
    elif 4.5 <= score <= 5:
        return "bardzo dobry"
    elif 4.0 <= score < 4.5:
        return "dobry"
    elif 3.0 <= score < 4.0:
        return "dostateczny"
    elif 2.0 <= score < 3.0:
        return "niedostateczny"
    else:
        return "Błędna ocena, wpisz od 2.0 do 5.0!"



if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

    print(temperature_and_air_pressure_check(0, 1013))
    print(temperature_and_air_pressure_check(1, 1013))
    print(temperature_and_air_pressure_check(0, 1014))
    print(temperature_and_air_pressure_check(1, 1014))

    print_the_value_of_speeding_fine_in_built_up_area(101)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(49)
    print_the_value_of_speeding_fine_in_built_up_area(50)
    print_the_value_of_speeding_fine_in_built_up_area(51)

    print(marks(1))
    print(marks(2))
    print(marks(3))
    print(marks(4.6))
    print(marks(5.5))
    print(marks(6))
    print(marks("Adrian"))

