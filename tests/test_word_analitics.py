from src.word_analitics import filter_words_containing_letter_a

[]
[1, 2, 3]
["@", "#"]
["pies", "kot", "smok"]
["b", "a"]


def test_filtering_words_empy_list():
    filtered_list = filter_words_containing_letter_a([])
    assert filtered_list == []


def test_filtering_words_for_list_of_integer():
    filtered_list = filter_words_containing_letter_a([1, 2, 3])
    assert filtered_list == []


def test_filtering_words_for_list_of_special_characters():
    filtered_list = filter_words_containing_letter_a(["@", "#"])
    assert filtered_list == []


def test_filtering_words_for_list_not_containing_any_word_with_letter_a():
    filtered_list = filter_words_containing_letter_a(["pies", "kot", "smok"])
    assert filtered_list == []


def test_filtering_words_for_list_for_mixed_list_of_sinlge_values():
    filtered_list = filter_words_containing_letter_a([1, "a", "b", "a"])
    assert filtered_list == ["a", "a"]


def test_filtering_words_for_list_of_all_words_containing_letter_a():
    filtered_list = filter_words_containing_letter_a(["paka", "nauka", "buka"])
    assert filtered_list == ["paka", "nauka", "buka"]


def test_filtering_words_for_list_for_mixed_list_of_words():
    filtered_list = filter_words_containing_letter_a(["kot", "żaba"])
    assert filtered_list == ["żaba"]
